#!/usr/bin/env bash
# IMPORTANT: run "dnf-setup.sh" as root user with "sudo su; ./dnf-setup.sh"
# before starting this script!



# add RPM Fusion
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y git mpv chocolate-doom terminator audacity \
	java-latest-openjdk keepassxc

# setup MS fonts
wget https://sourceforge.net/projects/mscorefonts2/files/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm/download -O msttcore-fonts-installer-2.6-1.noarch.rpm
sudo dnf install msttcore-fonts-installer-2.6-1.noarch.rpm -y

# setup minecraft
mkdir ~/.local/share/applications/
wget https://launcher.mojang.com/download/Minecraft.tar.gz
tar -xf Minecraft.tar.gz
sudo mv minecraft-launcher/minecraft-launcher /usr/bin
echo "[Desktop Entry]
Type=Application
Version=1.0
Name=Minecraft Launcher
Comment=Official Minecraft Launcher
Exec=/opt/minecraft-launcher/minecraft-launcher
Path=/opt/minecraft-launcher
Icon=minecraft-launcher
Terminal=false
Categories=Game;Application;" >> ~/.local/share/applications/minecraft-launcher.desktop
