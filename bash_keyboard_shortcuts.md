got this info from [this github page](https://gist.github.com/tuxfight3r/60051ac67c5f0445efee)


you can edit these by creating `~/.inputrc`

## Moving

| command           | description                 |
| ----------------- | --------------------------- |
| ctrl + a          | Goto beginning of line      |
| ctrl + e          | Goto end of line            |
| ctrl + b          | Move back 1 char            |
| ctrl + f          | Move fwd  1 char            |
| alt  + b          | Move back 1 word            |
| alt  + f          | Move fwd  1 word            |
| ctrl + ], x       | Move to next occurence of x |
| alt + ctrl + ], x | Move to prev occurence of x |

*Hint: if the keyboard does not have an alt key, instead of pressing alt, just press ESC before the desired action (and no need to hold it down, it acts as a toggle)*



