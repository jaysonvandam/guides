-----HOW TO ADD A WORKSPACE-----
1. System Settings>Workspace Behavior>Virtual Desktops
2. Click the "+ Add" button at bottom left
3. Name it "Desktop 2"
4. From now on, you can right click the pager widget at the bottom-left
and click "Add Virtual Desktop" if you want to add more



(pro-tip: press Ctrl+F2 to switch to workspace 2!)
