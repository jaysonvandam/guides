#!/usr/bin/env bash

# this script installs all of my programs using apt and sets them up.

# warning: still need to test on debian bullseye to make sure it works there
# as well.

# got these apt program names by doing:
# apt list --installed

# try to keep these in alphabetical order if you can.

sudo apt install -y audacity build-essential crispy-doom curl diffutils firefox\
flatpak findutils gimp git mpv obs-studio python3 qbittorrent synaptic\
terminator texlive-full ttf-mscorefonts-installer

# get ms cleartype fonts and other good shit
# (btw, this is definitely not gonna work on debian, so I'm gonna have to come up with a different way to get all restricted extras for debian.)
sudo add-apt-repository -y multiverse
# uncomment the line for your ubuntu spin before running the script
#sudo apt install -y ubuntu-restricted-extras
#sudo apt install -y kubuntu-restricted-extras
#sudo apt install -y lubuntu-restricted-extras
#sudo apt install -y xubuntu-restricted-extras

# setup flatpak support
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# clone dotfiles from gitlab account
git clone https://gitlab.com/jaysonvandam/dotfiles.git

# apps you need to add to /etc/apt/sources.list.d:
# element-desktop

# apps you need to get from deb files from developer's website:
# discord, zoom
