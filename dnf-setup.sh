#!/usr/bin/env bash
# IMPORTANT: Make sure to run this script as the root user
# by doing "sudo su" before running the script!
echo "fastestmirror=True" >> /etc/dnf/dnf.conf
dnf update -y
echo "IMPORTANT: Don't forget to reboot after running
the installer script!"
